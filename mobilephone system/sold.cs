using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;

namespace MOBILE
{
    public partial class sold : Form
    {
        public sold()
        {
            InitializeComponent();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.BackColor = Color.PowderBlue;
            button3.BackColor = Color.White;
            button5.BackColor = Color.White;

            // label9.Show();
            panel2.Show();
            panel3.Show();
            button7.Show();
            button8.Show();
            textBox8.Show();
            button15.Hide();
            button16.Hide();
            button13.Hide();
            button14.Hide();
            dataGridView1.Hide();
           // label10.Hide();
            panel4.Hide();
            textBox1.Text = null;
            textBox2.Text = null;
           
            textBox5.Text = null;
            textBox6.Text = null;
            textBox7.Text = null;
            textBox8.Text = null;
           


        }

        private void button3_Click(object sender, EventArgs e)
        {
            button3.BackColor = Color.PowderBlue;
            button1.BackColor = Color.White;
            button5.BackColor = Color.White;

            panel2.Show();
            panel3.Show();
            panel4.Show();
            label10.Hide();
            button15.Hide();
            button16.Hide();
            textBox8.Hide();
            button13.Show();
            button14.Show();
            button7.Hide();
            button8.Hide();
            dataGridView1.Show();
            label9.Show();
            textBox1.Text = null;
            textBox2.Text = null;
           
            textBox5.Text = null;
            textBox6.Text = null;
            textBox7.Text = null;
            textBox8.Text = null;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            button5.BackColor = Color.PowderBlue;
            button3.BackColor = Color.White;
            button1.BackColor = Color.White;

            panel2.Show();
            panel3.Show();
            panel4.Show();
            label10.Show();
            button15.Show();
            button16.Show();
            textBox8.Show();
            button13.Hide();
            button14.Hide();
            button7.Hide();
            button8.Hide();
            dataGridView1.Hide();
            label9.Hide();
            textBox1.Text = null;
            textBox2.Text = null;
            textBox5.Text = null;
            textBox6.Text = null;
            textBox7.Text = null;
            textBox8.Text = null;

        }

        private void sold_Load(object sender, EventArgs e)
        {
            panel1.Show();
            panel4.Hide();
            panel3.Hide();
            panel2.Hide();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {
            main m = new main();
            m.Show();
            Dispose();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button14_Click(object sender, EventArgs e)
        {

            SqlConnection con = new SqlConnection("Data Source=DESKTOP-F5PUFPH\\MSSQLSERVER2;Initial Catalog = mobilephone; Integrated Security = True");
            //con.Open();
            SqlDataAdapter sda = new SqlDataAdapter("select * from solds  where sold_id='" + textBox7.Text + "' ", con);
            //cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button7_Click(object sender, EventArgs e)
        {

            if (textBox1.Text == string.Empty || textBox2.Text == string.Empty || textBox5.Text == string.Empty || textBox6.Text==string.Empty)
            {

                MessageBox.Show("لطفااصلاعات را وارد کنید", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

            }

            SqlConnection con = new SqlConnection("Data Source=DESKTOP-F5PUFPH\\MSSQLSERVER2;Initial Catalog = mobilephone; Integrated Security = True");
            con.Open();
            SqlCommand cmd = new SqlCommand("insert into solds (costumer_id,code_product,number_sold,sale_month,sellerrr_id) values( '" + textBox1.Text + "' , '" + textBox2.Text + "'   , '" + textBox5.Text + "'  , '" + textBox6.Text + "' ,1 )", con);
            cmd.ExecuteNonQuery();
            textBox8.Text = "محصول با موفقیت به لیست فروش اضافه شد";
            con.Close();


        }

        private void button8_Click(object sender, EventArgs e)
        {

        }

        private void button15_Click(object sender, EventArgs e)
        {

            int i = 0;

            if (textBox7.Text == string.Empty)
            {

                MessageBox.Show("لطفا ماه موردنظر را وارد کنید", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

            }


            try
            {

                SqlConnection con = new SqlConnection("Data Source=DESKTOP-F5PUFPH\\MSSQLSERVER2;Initial Catalog = mobilephone; Integrated Security = True");

                SqlCommand com = new SqlCommand("SELECT COUNT(*) FROM soldds where  sale_month='" + textBox7.Text + "' ", con);

                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                    i = (int)com.ExecuteScalar();
                }//end if





               if(i<0)
                    textBox8.Text = "ماه موردنظر پیدا نشد";

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {
            
        }
    }
}
